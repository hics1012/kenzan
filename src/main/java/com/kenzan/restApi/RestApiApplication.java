package com.kenzan.restApi;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kenzan.restApi.model.Employee;

@SpringBootApplication
public class RestApiApplication {
	@Autowired
	EmployeeService employeeService;

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
	}

	@PostConstruct
	private void init() {
		ObjectMapper objectMapper = new ObjectMapper();
		List<Employee> employees;
		Resource resource = new ClassPathResource("employees.json");
		try {
			InputStream input = resource.getInputStream();
			File file = resource.getFile();
			employees = objectMapper.readValue(file, List.class);
			List<Employee> l = new LinkedList<Employee>();
			for(Object o : employees) {
				l.add(objectMapper.convertValue(o, Employee.class));
			}
			employeeService.createEmployees(l);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
