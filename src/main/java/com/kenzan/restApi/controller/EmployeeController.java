package com.kenzan.restApi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.kenzan.restApi.model.Employee;
import com.kenzan.restApi.model.EmployeeRepository;
import com.kenzan.restApi.model.Status;

@RestController
public class EmployeeController {

	@Autowired
	EmployeeRepository employeeRepo;

	@GetMapping("/employee/{ID}")
	public Employee getEmployee(@PathVariable Integer ID) {
		Optional<Employee> emp = employeeRepo.findByIDAndStatus(Integer.toUnsignedLong(ID), Status.ACTIVE);
		if (emp.isPresent()) {
			return emp.get();
		}

		return null;
	}
	@GetMapping("/employees")
	public List<Employee> getEmployees() {
		List<Employee> emp = employeeRepo.findAllByStatus(Status.ACTIVE);
		return emp;
	}
	@DeleteMapping(value = "/employee/{id}", produces = "application/json; charset=utf-8")
	public void deleteEmployee(@PathVariable Long id) {
		Optional<Employee> emp = employeeRepo.findById(id);
		if (emp.isPresent()) {
			Employee employee = emp.get();
			employee.disable();
			employeeRepo.save(employee);
		}
	}
	@PostMapping("/create/employee")
	public Employee addEmployee(@RequestBody Employee newEmployee) {
		Employee newEmp = new Employee(newEmployee.getDateOfBirth(), newEmployee.getDateOfEmployment(),
				newEmployee.getFirstName(), newEmployee.getLastName(), newEmployee.getMiddleInitial(), Status.ACTIVE);
		Employee newlyCreated = employeeRepo.save(newEmp);
		return newlyCreated;
	}
	@PostMapping("/update/employee")
	public Employee updateEmployee(@RequestBody Employee newEmployee) {
		Optional<Employee> emp = employeeRepo.findById(newEmployee.getId());
		if (!emp.isPresent()) {
			return null;
		}
		Employee dbEmp = emp.get();
		dbEmp.update(newEmployee);
		return employeeRepo.save(dbEmp);
	}


}