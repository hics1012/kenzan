package com.kenzan.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kenzan.restApi.model.Employee;
import com.kenzan.restApi.model.EmployeeRepository;

@Component
public class EmployeeService {
	@Autowired
	EmployeeRepository empRepo;
	
	public void createEmployees(List<Employee> employees) {
		for(Employee e: employees){
			empRepo.save(e);
		}
	}

}
