package com.kenzan.restApi.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

	Optional<Employee> findByIDAndStatus(long unsignedLong, Status active);

	List<Employee> findAllByStatus(Status active);
	
}