package com.kenzan.restApi.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Entity
public class Employee {
	@JsonProperty
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private @NonNull Long ID;

	@JsonProperty
	@NonNull
	private String firstName;


	@JsonProperty
	@NonNull
	private String middleInitial;
	
	@JsonProperty
	@NonNull
	private String lastName;

	@JsonProperty
	private @NonNull Date dateOfBirth;
	
	@JsonProperty
	private @NonNull Date dateOfEmployment;
	
	@JsonProperty
	@Enumerated(EnumType.STRING)
	private @NonNull Status status;
	
	public Employee(){}
	
	public Employee(Date dob, Date doe, String firstName, String lastName, String middleInitial, Status status) {
		this.dateOfBirth = dob;
		this.dateOfEmployment = doe;
		this.firstName = firstName;
		this.lastName = lastName;
		this.middleInitial = middleInitial;
		this.status = status;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getMiddleInitial() {
		return middleInitial;
	}
	
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
	public Date getDateOfEmployment() {
		return dateOfEmployment;
	}
	
	public void setDateOfEmployment(Date dateOfEmployment) {
		this.dateOfEmployment = dateOfEmployment;
	}
	
	public Status getStatus() {
		return status;
	}
	public Long getId() {
		return ID;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}
	public void update(Employee emp) {
		this.dateOfBirth = emp.dateOfEmployment;
		this.dateOfEmployment = emp.dateOfEmployment;
		this.firstName = emp.firstName;
		this.lastName = emp.lastName;
		this.middleInitial = emp.middleInitial;
		this.status = emp.status;
	}
	public void disable() {
		this.status = Status.INACTIVE;
	}
}