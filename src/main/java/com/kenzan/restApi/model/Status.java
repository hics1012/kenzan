package com.kenzan.restApi.model;

public enum Status {
	ACTIVE, INACTIVE
}
